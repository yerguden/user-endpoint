import { NextFunction, Request, Response } from 'express';

export default (err: any, req: Request, res: Response, next: NextFunction) => {
  if (res.headersSent) {
    return next(err);
  }

  if ((err.code && err.code === 11000) || err.name === 'ValidationError') {
    // todo: normalize ugly mongodb errors
    return res.status(400).send(err.message);
  }

  return res.status(500).send('server error');
};
