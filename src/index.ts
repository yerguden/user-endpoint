import express from 'express';
import mongoose from 'mongoose';
import config from 'config';
import userRouter from './routes/user';
import errorHandler from './middlewares/errorHandler';

const app = express();
const port = process.env.PORT || '8000';

app.use('/users', userRouter);
app.use(errorHandler);

(async () => {
  await mongoose.connect(config.get<string>('db_config.uri'), { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false });
  app.listen(port, (): void => console.log(`Server is listening on ${port}`));
})();
