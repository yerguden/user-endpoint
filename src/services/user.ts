import User from 'user';
import UserModel from '../models/user';

export const createUser = async (user: User) => UserModel.create(user);

export const listUsers = async () => UserModel.find();

export const getUserById = (userId: string) => UserModel.findOne({ _id: userId });

export const deleteUser = (userId: string) => UserModel.findByIdAndDelete({ _id: userId });

export const updateUser = (userId: string, updateObj: Partial<User>) => UserModel
  .findByIdAndUpdate(userId, updateObj, { new: true });
