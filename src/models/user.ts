import { pick } from 'lodash';
import Mongoose, { Document } from 'mongoose';
import validator from 'validator';
import User from '../types/user';

const pickUserFields = (user: User): User => pick(user, ['_id', 'email', 'givenName', 'familyName', 'created']);

interface UserDocument extends User, Document<string> {
  _id: string;
}

const userSchema = new Mongoose.Schema({
  email: {
    type: String,
    required: true,
    unique: true,
    validate: [validator.isEmail, 'invalid email'],
  },
  givenName: {
    type: String,
    required: true,
    validate: [validator.isAlpha, 'invalid givenName'],
  },
  familyName: {
    type: String,
    required: true,
    validate: [validator.isAlpha, 'invalid familyName'],
  },
}, { timestamps: { createdAt: 'created' } });

userSchema.set('toJSON', {
  transform(doc: UserDocument, ret: User) {
    return pickUserFields(ret);
  },
});

export default Mongoose.model<UserDocument>('User', userSchema);
