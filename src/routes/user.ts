import express from 'express';
import { pick } from 'lodash';
import { query } from 'express-validator';
import {
  createUser, deleteUser, getUserById, listUsers, updateUser,
} from '../services/user';
import User from '../types/user';

const userRouter = express.Router();
const userFields = ['email', 'givenName', 'familyName'];

userRouter.get('/', async (req, res, next) => {
  try {
    res.json(await listUsers());
  } catch (error) {
    next(error);
  }
});

userRouter.post(
  '/',
  query('email').not().isEmpty().normalizeEmail(),
  query('givenName').not().isEmpty().isAlpha()
    .trim(),
  query('familyName').not().isEmpty().isAlpha()
    .trim(),
  async (req, res, next) => {
    try {
      const user = pick(req.query, userFields) as unknown as User;

      res.json(await createUser(user));
    } catch (error) {
      next(error);
    }
  },
);

userRouter.get('/:userId', async (req, res, next) => {
  try {
    const { userId } = req.params;
    const user = await getUserById(userId);

    if (!user) {
      return res.status(404).send();
    }

    return res.send(user);
  } catch (error) {
    next(error);
  }
});

userRouter.put(
  '/:userId',
  query('email').optional().normalizeEmail(),
  query('givenName').optional().not().isEmpty()
    .isAlpha()
    .trim(),
  query('familyName').optional().not().isEmpty()
    .isAlpha()
    .trim(),
  async (req, res, next) => {
    try {
      const { userId } = req.params;
      const updateFields: Partial<User> = pick(req.query, userFields);

      const user = await updateUser(userId, updateFields);

      if (!user) {
        return res.status(404).send();
      }

      return res.send(user);
    } catch (error) {
      next(error);
    }
  },
);

userRouter.delete('/:userId', async (req, res, next) => {
  try {
    const { userId } = req.params;
    await deleteUser(userId);

    res.status(200).send();
  } catch (error) {
    next(error);
  }
});

export default userRouter;
