export default interface User {
  email: string,
  givenName: string,
  familyName: string,
  _id: string,
  created: string,
}
