# User Endpoint
Mongodb should be up and running before starting the project.

Easiest way to get started should be as running:

```
yarn && yarn build && yarn start
```

or for development
```
yarn && yarn dev
```

Feel free to use postman collection User.postman_collection.json for easier testing.
